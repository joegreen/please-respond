import React from 'react';
import ndjsonStream from 'can-ndjson-stream';
import { countBy, sortBy } from 'lodash';
import { epochToLocal } from './utils/time';

class App extends React.Component {
  // State
  state = {
    results: null,
    latestTime: null,
    latestEventURL: null
  }

  // Functions
  fetchStream = async () => {
    // not meant for production. Used because of CORs issue.
    const proxyurl = 'https://cors-anywhere.herokuapp.com/';
    const url = 'http://stream.meetup.com/2/rsvps'
    const response = await fetch(proxyurl + url);
    const reader = ndjsonStream(response.body).getReader();
    const data = [];
    let result;

    while (!result || !result.done) {
      result = await reader.read();
      data.push(result)
      this.setState({ results: data });
    }
  }

  getLatestEventInfo = () => {
    const { results } = this.state;

    const latestEvent = sortBy(results.map(result => result.value.event), ['time']).pop();
    const latestTime = epochToLocal(latestEvent.time, 'YYYY-MM-DD hh:mm');
    const latestEventURL = latestEvent['event_url'];

    return [latestTime, latestEventURL];
  }

  getTopCountries = () => {
    const { results } = this.state;

    const countriesByCount = countBy(results.map(result => result.value.group['group_country']));

    const countryKeys = Object.keys(countriesByCount).map(key => {
      return { key, value: countriesByCount[key] }
    }, countriesByCount);

    return sortBy(countryKeys, ['value']).reverse().splice(0, 3);

  }

  getCount = () => {
    const { results } = this.state;
    return results.length;
  }

  formattedOutput = () => {
    if (this.state.results) {
      const count = this.getCount();
      const [date, url] = this.getLatestEventInfo();
      const top3Countries = this.getTopCountries().map(country => `${country.key},${country.value}`);
      const output = `${count},${date},${url},${top3Countries}`;

      return output;
    }

    return null;

  }

  // Lifecycle
  componentDidMount() {
    this.fetchStream();
  }

  render() {
    return (
      <>
        <h1>Output</h1>
        <>
          {this.formattedOutput()}
        </>
      </>
    );
  }
}

export default App;
