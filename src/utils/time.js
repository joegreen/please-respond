import moment from 'moment';
import Proptypes from 'prop-types';

// Takes an epoch string and formats it.
// See https://momentjs.com/docs/#/parsing/string-format/ for formatting styles.
export const epochToLocal = (epoch, format) => {
  return moment(epoch).format(format);
}

epochToLocal.Proptypes = {
  epoch: Proptypes.string,
  format: Proptypes.string
}